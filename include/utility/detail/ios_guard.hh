#pragma once
#include <ios>

namespace CodeLab::detail {

template <typename CharT, typename Traits>
class FlagsGuard
{
public:
    using StreamType = std::basic_ios<CharT, Traits>;

    FlagsGuard(const FlagsGuard &) = delete;
    FlagsGuard &operator=(const FlagsGuard &) = delete;

    explicit FlagsGuard(StreamType &ios) :
        stream(ios),
        fmt_flags(stream.flags())
    {
    }

    ~FlagsGuard()
    {
        this->stream.flags(this->fmt_flags);
    }

private:
    StreamType &stream;
    StreamType::fmtflags fmt_flags;
};

template <typename CharT, typename Traits>
class FillGuard
{
public:
    using StreamType = std::basic_ios<CharT, Traits>;

    FillGuard(const FillGuard &) = delete;
    FillGuard &operator=(const FillGuard &) = delete;

    explicit FillGuard(StreamType &ios) : stream(ios), fill_char(stream.fill())
    {
    }

    ~FillGuard()
    {
        this->stream.fill(this->fill_char);
    }

private:
    StreamType &stream;
    StreamType::char_type fill_char;
};

} // namespace CodeLab::detail
