#pragma once
#include <functional>
#include <type_traits>

namespace CodeLab::detail {

template <typename T>
struct IsSimpleComparator : std::false_type
{};
template <typename T>
struct IsSimpleComparator<std::equal_to<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::equal_to<>> : std::true_type
{};
template <typename T>
struct IsSimpleComparator<std::not_equal_to<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::not_equal_to<>> : std::true_type
{};
template <typename T>
struct IsSimpleComparator<std::greater<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::greater<>> : std::true_type
{};
template <typename T>
struct IsSimpleComparator<std::less<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::less<>> : std::true_type
{};
template <typename T>
struct IsSimpleComparator<std::greater_equal<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::greater_equal<>> : std::true_type
{};
template <typename T>
struct IsSimpleComparator<std::less_equal<T>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::less_equal<>> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::equal_to> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::not_equal_to> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::greater> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::less> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::greater_equal> : std::true_type
{};
template <>
struct IsSimpleComparator<std::ranges::less_equal> : std::true_type
{};
template <>
struct IsSimpleComparator<std::compare_three_way> : std::true_type
{};

template <typename Iterator>
inline constexpr bool is_simple_comparator =
    IsSimpleComparator<Iterator>::value;

} // namespace CodeLab::detail
