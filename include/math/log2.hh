#pragma once
#include <bit>
#include <concepts>
#include <type_traits>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Binary_logarithm
template <std::integral T>
[[nodiscard]] constexpr int log2i(T n) noexcept
{
    return n == 0 ? 0 : std::bit_width<std::make_unsigned_t<T>>(n) - 1;
}

} // namespace CodeLab
