#pragma once
#include <bit>
#include <concepts>
#include <cstdint>
#include <limits>

namespace CodeLab {

namespace detail {

// https://en.wikipedia.org/wiki/Fast_inverse_square_root
template <std::floating_point T, std::unsigned_integral U>
    requires(std::numeric_limits<T>::is_iec559 && sizeof(T) == sizeof(U))
constexpr T do_rsqrt(T n, U x, T y, T z) noexcept
{
    auto u = std::bit_cast<U>(n) >> 1;
    auto f = std::bit_cast<T>(x - u);
    return f * y * (z - n * f * f);
}

} // namespace detail

[[nodiscard]] constexpr float rsqrt(float n) noexcept
{
    return detail::do_rsqrt<float, std::uint32_t>(n, 0x5F375A86, 0.5f, 3.0f);
}

[[nodiscard]] constexpr double rsqrt(double n) noexcept
{
    return detail::do_rsqrt<double, std::uint64_t>(n, 0x5FE6EB50C7B537A9, 0.5,
                                                   3.0);
}

// http://rrrola.wz.cz/inv_sqrt.html
[[nodiscard]] constexpr float inv_sqrt(float n) noexcept
{
    return detail::do_rsqrt<float, std::uint32_t>(n, 0x5F1FFFF9, 0.703952253f,
                                                  2.38924456f);
}

} // namespace CodeLab
