#pragma once
#include <concepts>
#include <type_traits>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Absolute_value
template <std::integral T>
    requires(!std::is_same_v<T, bool>)
[[nodiscard]] constexpr std::make_unsigned_t<T> absu(T n)
{
    // Return |n| and cast the type to unsigned.
    if constexpr (std::is_signed_v<T>)
        n = n < 0 ? -n : n;
    return n;
}

} // namespace CodeLab
