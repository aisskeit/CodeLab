#pragma once
#include "abs.hh"
#include <bit>
#include <concepts>
#include <type_traits>
#include <utility>

namespace CodeLab {

namespace detail {

// https://en.wikipedia.org/wiki/Binary_GCD_algorithm
template <typename T>
constexpr T do_gcd(T m, T n) noexcept
{
    int i = std::countr_zero(m), j = std::countr_zero(n),
        k = i < j ? i : j; // k = std::min(i, j)
    m >>= i;
    n >>= j;
    while (true) {
        if (m > n)
            std::swap(m, n);
        n -= m;
        if (n == 0)
            return m << k;
        n >>= std::countr_zero(n);
    }
}

} // namespace detail

// https://en.wikipedia.org/wiki/Greatest_common_divisor
template <std::integral T, std::integral U>
    requires(!std::is_same_v<std::remove_cv_t<T>, bool> &&
             !std::is_same_v<std::remove_cv_t<U>, bool>)
[[nodiscard]] constexpr auto gcd(T m, U n) noexcept
{
    using CommonType = std::common_type_t<T, U>;
    using UnsignedType = std::make_unsigned_t<CommonType>;
    if (m == 0)
        return n;
    if (n == 0)
        return m;
    return static_cast<CommonType>(
        detail::do_gcd<UnsignedType>(absu(m), absu(n)));
}

// https://en.wikipedia.org/wiki/Least_common_multiple
template <std::integral T, std::integral U>
    requires(!std::is_same_v<std::remove_cv_t<T>, bool> &&
             !std::is_same_v<std::remove_cv_t<U>, bool>)
[[nodiscard]] constexpr auto lcm(T m, U n) noexcept
{
    using CommonType = std::common_type_t<T, U>;
    using UnsignedType = std::make_unsigned_t<CommonType>;
    if (m == 0 || n == 0)
        return 0;
    UnsignedType a = absu(m), b = absu(n);
    return static_cast<CommonType>(a / detail::do_gcd(a, b) * b);
}

} // namespace CodeLab
