#pragma once
#include "../math/log2.hh"
#include "detail/sort_n.hh"
#include "detail/unguarded_insertion_sort.hh"
#include "detail/unguarded_partition.hh"
#include "heap_sort.hh"
#include "insertion_sort.hh"
#include <functional>
#include <iterator>

namespace CodeLab {

namespace detail {

template <typename T>
inline constexpr T introsort_threshold = 16;

template <typename Iterator, typename Compare>
constexpr void introsort_loop(Iterator first, Iterator last, Compare comp,
                              int depth_limit)
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    constexpr DifferenceType one = 1;
    for (DifferenceType length;
         (length = last - first) > introsort_threshold<DifferenceType>;) {
        if (depth_limit == 0) [[unlikely]] {
            // When the depth of recursion exceeds depth_limit, switches to
            // heapsort to ensure an O(n log n) complexity.
            heap_sort(first, last, comp);
            return;
        }
        // median of three
        sort3(first + length / DifferenceType(2), first, last - one, comp);
        Iterator i = unguarded_partition(first, last, comp);
        introsort_loop(i + one, last, comp, --depth_limit);
        last = i;
    }
}

} // namespace detail

// https://en.wikipedia.org/wiki/Introsort
template <typename Iterator, typename Compare = std::less<>>
constexpr void introsort(Iterator first, Iterator last, Compare comp = {})
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    DifferenceType length = last - first;
    detail::introsort_loop(first, last, comp, log2i(length) * 2);
    if (length > detail::introsort_threshold<DifferenceType>) {
        Iterator i = first + detail::introsort_threshold<DifferenceType>;
        insertion_sort(first, i, comp);
        detail::unguarded_insertion_sort(i, last, comp);
    } else {
        insertion_sort(first, last, comp);
    }
}

} // namespace CodeLab
