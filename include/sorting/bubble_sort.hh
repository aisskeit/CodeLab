#pragma once
#include <algorithm>
#include <functional>
#include <iterator>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Bubble_sort
template <typename Iterator, typename Compare = std::less<>>
constexpr void bubble_sort(Iterator first, Iterator last, Compare comp = {})
{
    bool swapped = true; // Used to indicate whether an exchange has occurred.
    while (first != last && swapped) {
        swapped = false;
        for (Iterator i = std::next(first); i != last; ++i) {
            Iterator prev_i = std::prev(i);
            if (comp(*i, *prev_i)) {
                std::iter_swap(i, prev_i);
                swapped = true;
            }
        }
        --last;
    }
}

} // namespace CodeLab
