#pragma once
#include "../math/log2.hh"
#include "detail/insertion_sort_incomplete.hh"
#include "detail/sort_n.hh"
#include "detail/unguarded_insertion_sort.hh"
#include "detail/unguarded_partition_equal.hh"
#include "detail/use_branchless_partition.hh"
#include "heap_sort.hh"
#include "insertion_sort.hh"
#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <utility>

namespace CodeLab {

namespace detail::pdqsort {

template <typename T>
inline constexpr T insertion_sort_threshold = 24;
template <typename T>
inline constexpr T ninther_threshold = 128;
inline constexpr std::size_t block_size = 64;
inline constexpr std::size_t cacheline_size = 64;

template <typename Iterator, typename Distance>
constexpr void break_patterns(Iterator first, Iterator i, Iterator last,
                              Distance left_length, Distance right_length)
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    constexpr DifferenceType one = 1, two = 2, three = 3, four = 4;
    if (left_length >= insertion_sort_threshold<DifferenceType>) {
        std::iter_swap(first, first + left_length / four);
        std::iter_swap(i - one, i - left_length / four);
        if (left_length > ninther_threshold<DifferenceType>) {
            std::iter_swap(first + one, first + (left_length / four + one));
            std::iter_swap(first + two, first + (left_length / four + two));
            std::iter_swap(i - two, i - (left_length / four + one));
            std::iter_swap(i - three, i - (left_length / four + two));
        }
    }
    if (right_length >= insertion_sort_threshold<DifferenceType>) {
        std::iter_swap(i + one, i + (right_length / four + one));
        std::iter_swap(last - one, last - right_length / four);
        if (right_length > ninther_threshold<DifferenceType>) {
            std::iter_swap(i + two, i + (right_length / four + two));
            std::iter_swap(i + three, i + (right_length / four + three));
            std::iter_swap(last - two, last - (right_length / four + one));
            std::iter_swap(last - three, last - (right_length / four + two));
        }
    }
}

template <typename Iterator, typename Compare, typename Distance>
constexpr void choose_pivot(Iterator first, Iterator last, Compare comp,
                            Distance length)
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    constexpr DifferenceType one = 1, two = 2;
    Iterator middle = first + length / two;
    sort3(middle, first, last - one, comp);
    if (length > ninther_threshold<DifferenceType>) {
        // Tukey's ninther
        // https://doi.org/10.1016/B978-0-12-204750-3.50024-1
        sort3(first + one, middle - one, last - two, comp);
        sort3(first + two, middle + one, last - DifferenceType(3), comp);
        sort3(middle - one, first, middle + one, comp);
    }
}

template <bool branchless, typename Iterator, typename Compare>
constexpr std::pair<Iterator, bool>
unguarded_partition(Iterator first, Iterator last, Compare comp)
{
    Iterator pivot_pos = first;
    auto pivot = std::move(*pivot_pos);
    // Exclude elements that are already in the correct position.
    while (comp(*++first, pivot)) {}
    while (comp(pivot, *--last)) {}
    bool no_swap = first >= last;
    while (first < last) {
        std::iter_swap(first, last);
        while (comp(*++first, pivot)) {}
        while (comp(pivot, *--last)) {}
    }
    *pivot_pos = std::move(*--first);
    *first = std::move(pivot);
    return std::make_pair(first, no_swap);
}

template <bool branchless, typename Iterator, typename Compare>
constexpr void pdqsort_loop(Iterator first, Iterator last, Compare comp,
                            int bad_allowed, bool leftmost)
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    constexpr DifferenceType one = 1;
    for (DifferenceType length;
         (length = last - first) >= insertion_sort_threshold<DifferenceType>;) {
        choose_pivot(first, last, comp, length);
        if (!leftmost && !comp(first[DifferenceType(-1)], *first)) {
            first = unguarded_partition_equal(first, last, comp);
            continue;
        }
        auto [i, perfect_part] =
            unguarded_partition<branchless>(first, last, comp);
        auto left_length = i - first, right_length = last - i,
             limit_length = length / DifferenceType(8);
        if (left_length < limit_length || right_length < limit_length)
            [[unlikely]] {
            if (--bad_allowed == 0) [[unlikely]] {
                heap_sort(first, last, comp);
                return;
            }
            break_patterns(first, i, last, left_length, right_length);
        } else if (perfect_part) {
            // DIFFERENT FORM ORLP'S PDQSORT
            // The handling of perfect partition is the same as
            // std::__1::__introsort() in the libc++ library.
            bool left_ordered = insertion_sort_incomplete(first, i, comp),
                 right_ordered = insertion_sort_incomplete(i + one, last, comp);
            if (right_ordered) {
                if (left_ordered)
                    return;
                last = i;
                continue;
            } else if (left_ordered) {
                first = i + one;
                leftmost = false;
                continue;
            }
        }
        pdqsort_loop<branchless>(first, i, comp, bad_allowed, leftmost);
        first = i + one;
        leftmost = false;
    }
    if (leftmost)
        insertion_sort(first, last, comp);
    else
        unguarded_insertion_sort(first, last, comp);
}

} // namespace detail::pdqsort

// https://en.wikipedia.org/wiki/Introsort#pdqsort
template <typename Iterator, typename Compare = std::less<>>
constexpr void pdqsort(Iterator first, Iterator last, Compare comp = {})
{
    detail::pdqsort::pdqsort_loop<
        detail::use_branchless_partition<Iterator, Compare>>(
        first, last, comp, log2i(last - first), true);
}

} // namespace CodeLab
