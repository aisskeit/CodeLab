#pragma once
#include <algorithm>
#include <functional>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Selection_sort
template <typename Iterator, typename Compare = std::less<>>
constexpr void selection_sort(Iterator first, Iterator last, Compare comp = {})
{
    for (; first != last; ++first)
        std::iter_swap(first, std::min_element(first, last, comp));
}

} // namespace CodeLab
