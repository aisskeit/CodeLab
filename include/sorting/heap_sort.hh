#pragma once
#include <algorithm>
#include <functional>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Heapsort
template <typename Iterator, typename Compare = std::less<>>
constexpr void heap_sort(Iterator first, Iterator last, Compare comp = {})
{
    std::make_heap(first, last, comp);
    std::sort_heap(first, last, comp);
}

} // namespace CodeLab
