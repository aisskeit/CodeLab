#pragma once
#include <algorithm>
#include <iterator>
#include <utility>

namespace CodeLab::detail {

template <typename Iterator, typename Compare>
constexpr bool insertion_sort_incomplete(Iterator first, Iterator last,
                                         Compare comp)
{
    unsigned count = 0;
    for (Iterator i = std::next(first); i != last;) {
        Iterator prev_i = std::prev(i), next_i = std::next(i);
        if (comp(*i, *prev_i)) {
            auto temp = std::move(*i);
            if (comp(*i, *first)) {
                std::move_backward(first, i, next_i);
                *first = std::move(temp);
            } else {
                Iterator j = i;
                do {
                    *j = std::move(*prev_i);
                    j = prev_i;
                } while (comp(temp, *--prev_i));
                *j = std::move(temp);
            }
            if (++count == 8u)
                return ++i == last;
        }
        i = next_i;
    }
    return true;
}

} // namespace CodeLab::detail
