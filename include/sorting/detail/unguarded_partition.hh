#pragma once
#include <algorithm>
#include <utility>

namespace CodeLab::detail {

template <typename Iterator, typename Compare>
constexpr Iterator unguarded_partition(Iterator first, Iterator last,
                                       Compare comp)
{
    Iterator pivot_pos = first;
    auto pivot = std::move(*pivot_pos);
    while (true) {
        while (comp(*++first, pivot)) {}
        while (comp(pivot, *--last)) {}
        if (first >= last) {
            *pivot_pos = std::move(*--first);
            *first = std::move(pivot);
            return first;
        }
        std::iter_swap(first, last);
    }
}

} // namespace CodeLab::detail
