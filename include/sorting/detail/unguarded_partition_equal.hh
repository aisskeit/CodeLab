#pragma once
#include <algorithm>
#include <utility>

namespace CodeLab::detail {

template <typename Iterator, typename Compare>
constexpr Iterator unguarded_partition_equal(Iterator first, Iterator last,
                                             Compare comp)
{
    Iterator pivot_pos = first;
    auto pivot = std::move(*pivot_pos);
    if (comp(pivot, *--last)) {
        while (comp(pivot, *--last)) {}
        while (!comp(pivot, *++first)) {}
    } else {
        do
            if (++first == last)
                return ++first;
        while (!comp(pivot, *first));
    }
    while (first < last) {
        std::iter_swap(first, last);
        while (comp(pivot, *--last)) {}
        while (!comp(pivot, *++first)) {}
    }
    *pivot_pos = std::move(pivot);
    return first;
}

} // namespace CodeLab::detail
