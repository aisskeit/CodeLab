#pragma once
#include <iterator>
#include <type_traits>

namespace CodeLab::detail {

template <typename Iterator, typename Compare>
inline constexpr bool use_branchless_partition =
    std::contiguous_iterator<Iterator> &&
    std::is_scalar_v<std::iter_value_t<Iterator>>;

} // namespace CodeLab::detail
