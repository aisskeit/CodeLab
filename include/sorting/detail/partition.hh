#pragma once
#include <algorithm>
#include <iterator>
#include <utility>

namespace CodeLab::detail {

template <typename Iterator, typename T, typename Predicate>
constexpr Iterator do_partition(Iterator first, Iterator last, T &pivot,
                                Predicate pred)
{
    if constexpr (std::bidirectional_iterator<Iterator>) {
        // https://en.wikipedia.org/wiki/Quicksort#Hoare_partition_scheme
        while (true) {
            do
                if (++first == last)
                    return --first;
            while (pred(*first, pivot));
            do
                if (first == --last)
                    return --first;
            while (!pred(*last, pivot));
            std::iter_swap(first, last);
        }
    } else {
        // https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme
        Iterator i = first;
        for (Iterator j = ++first; j != last; ++j) {
            if (pred(*j, pivot)) {
                std::iter_swap(first, j);
                i = first;
                ++first;
            }
        }
        return i;
    }
}

template <typename Iterator, typename Predicate>
constexpr Iterator partition(Iterator first, Iterator last, Predicate pred)
{
    auto pivot = std::move(*first);
    Iterator i = do_partition(first, last, pivot, pred);
    *first = std::move(*i);
    *i = std::move(pivot);
    return i;
}

} // namespace CodeLab::detail
