#pragma once
#include <iterator>
#include <utility>

namespace CodeLab::detail {

template <typename Iterator, typename Compare>
constexpr void unguarded_insertion_sort(Iterator first, Iterator last,
                                        Compare comp)
{
    while (first != last) {
        Iterator i = std::prev(first);
        if (comp(*first, *i)) {
            Iterator j = first;
            auto temp = std::move(*j);
            do {
                *j = std::move(*i);
                j = i;
            } while (comp(temp, *--i));
            *j = std::move(temp);
        }
        ++first;
    }
}

} // namespace CodeLab::detail
