#pragma once
#include <algorithm>
#include <functional>
#include <iterator>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Gnome_sort
template <typename Iterator, typename Compare = std::less<>>
constexpr void gnome_sort(Iterator first, Iterator last, Compare comp = {})
{
    // Here is how a garden gnome sorts a line of flower pots:
    // Basically, he looks at the flower pot next to him and the previous one;
    // if they are in the right order he steps one pot forward, otherwise he
    // swaps them and steps one pot backwards.
    // Boundary conditions:
    // 1.If there is no previous pot, he steps forwards.
    // 2.If there is no pot next to him, he is done.
    for (Iterator i = first; i != last;) {
        Iterator prev_i = std::prev(i);
        if (i == first || !comp(*i, *prev_i)) {
            ++i;
        } else {
            std::iter_swap(i, prev_i);
            i = prev_i;
        }
    }
}

} // namespace CodeLab
