#pragma once
#include <algorithm>
#include <functional>
#include <iterator>
#include <utility>

namespace CodeLab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void binary_sort(Iterator first, Iterator last, Compare comp = {})
{
    if (first == last)
        return;
    for (Iterator i = std::next(first); i != last;) {
        Iterator next_i = std::next(i);
        if (comp(*i, *std::prev(i))) {
            auto temp = std::move(*i);
            Iterator j = std::upper_bound(first, i, temp, comp);
            std::move_backward(j, i, next_i);
            *j = std::move(temp);
        }
        i = next_i;
    }
}

} // namespace CodeLab
