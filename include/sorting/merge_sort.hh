#pragma once
#include <algorithm>
#include <functional>
#include <iterator>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Merge_sort
template <typename Iterator, typename Compare = std::less<>>
void merge_sort(Iterator first, Iterator last, Compare comp = {})
{
    using DifferenceType = std::iter_difference_t<Iterator>;
    constexpr DifferenceType two = 2;
    auto length = std::distance(first, last);
    if (length < two)
        return;
    Iterator middle = std::next(first, length / two);
    // The unsorted range is divided into n subranges, each containing one
    // element (a range with only one element is considered already sorted).
    merge_sort(first, middle, comp);
    merge_sort(middle, last, comp);
    // Repeatedly merge subranges to produce a new sorted subrange until one
    // subrange remains. This final subrange represents the sorted range.
    std::inplace_merge(first, middle, last, comp);
}

} // namespace CodeLab
