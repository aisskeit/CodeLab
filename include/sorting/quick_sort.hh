#pragma once
#include "detail/partition.hh"
#include <functional>
#include <iterator>

namespace CodeLab {

// https://en.wikipedia.org/wiki/Quicksort
template <typename Iterator, typename Compare = std::less<>>
constexpr void quick_sort(Iterator first, Iterator last, Compare comp = {})
{
    auto length = std::distance(first, last);
    if (length < std::iter_difference_t<Iterator>(2))
        return;
    Iterator i = detail::partition(first, last, comp);
    quick_sort(first, i, comp);
    quick_sort(std::next(i), last, comp);
}

} // namespace CodeLab
