#pragma once
#include "detail/xorshift_engine.hh"
#include <cstdint>

namespace CodeLab {

using Xorshift8 = detail::XorshiftEngine<std::uint8_t, 3, 1, 5>;
using Xorshift16 = detail::XorshiftEngine<std::uint16_t, 7, 9, 8>;
using Xorshift32 = detail::XorshiftEngine<std::uint32_t, 13, 17, 5>;
using Xorshift64 = detail::XorshiftEngine<std::uint64_t, 13, 7, 17>;

} // namespace CodeLab
