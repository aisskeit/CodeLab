#pragma once
#include <cstdint>
#include <random>

namespace CodeLab {

using GlibcRand48 = std::linear_congruential_engine<std::uint64_t, 0x5DEECE66Du,
                                                    0xBu, 0x1000000000000u>;

} // namespace CodeLab
