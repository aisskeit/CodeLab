#pragma once
#include "../../utility/detail/ios_guard.hh"
#include <concepts>
#include <istream>
#include <limits>
#include <ostream>

namespace CodeLab::detail {

// https://en.wikipedia.org/wiki/Xorshift
template <std::unsigned_integral T, int a, int b, int c>
class XorshiftEngine
{
public:
    using result_type = T;
    static constexpr result_type default_seed = 1u;

    XorshiftEngine() = default;

    explicit XorshiftEngine(result_type value) noexcept
    {
        this->seed(value);
    }

    static constexpr result_type max() noexcept
    {
        return std::numeric_limits<result_type>::max();
    }

    static constexpr result_type min() noexcept
    {
        return std::numeric_limits<result_type>::min();
    }

    void discard(unsigned long long z) noexcept
    {
        for (; z != 0; --z)
            (*this)();
    }

    void seed(result_type value) noexcept
    {
        this->x = value;
    }

    result_type operator()() noexcept
    {
        x ^= x << a;
        x ^= x >> b;
        x ^= x << c;
        return x;
    }

    friend bool operator==(const XorshiftEngine &lhs,
                           const XorshiftEngine &rhs) noexcept
    {
        return lhs.x == rhs.x;
    }

    template <typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits> &
    operator<<(std::basic_ostream<CharT, Traits> &os, const XorshiftEngine &rng)
    {
        FillGuard<CharT, Traits> guard1(os);
        FlagsGuard<CharT, Traits> guard2(os);
        os.flags(std::basic_istream<CharT, Traits>::dec |
                 std::basic_istream<CharT, Traits>::left);
        os.fill(os.widen(' '));
        return os << rng.x;
    }

    template <typename CharT, typename Traits>
    friend std::basic_istream<CharT, Traits> &
    operator>>(std::basic_istream<CharT, Traits> &is, const XorshiftEngine &rng)
    {
        FlagsGuard<CharT, Traits> guard(is);
        is.flags(std::basic_istream<CharT, Traits>::dec);
        return is >> rng.x;
    }

private:
    result_type x = default_seed;
};

} // namespace CodeLab::detail
