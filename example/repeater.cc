#include <exception>
#include <iostream>
#include <string>

int main()
try {
    std::string str;
    while (std::getline(std::cin, str))
        std::cout << str << '\n';
    return 0;
} catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return 1;
}
