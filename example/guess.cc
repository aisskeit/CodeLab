#include <exception>
#include <iostream>
#include <random>

int main()
try {
    using ResultType = std::random_device::result_type;
    const ResultType lower = 1u, upper = 100u;
    std::random_device rng;
    std::uniform_int_distribution<ResultType> uni(lower, upper);
    ResultType target = uni(rng), guess;
    std::cin.exceptions(std::istream::failbit);
    std::cout << "Guess a number between " << lower << " and " << upper
              << ".\n";
    for (ResultType tries = 0;; tries++) {
        std::cin >> guess;
        if (guess > target) {
            std::cout << "Try lower.\n";
        } else if (guess < target) {
            std::cout << "Try higher.\n";
        } else {
            std::cout << "Correct! You got it in " << tries << " guesses.\n";
            break;
        }
    }
    return 0;
} catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return 1;
}
