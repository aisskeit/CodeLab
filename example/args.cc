#include <exception>
#include <iostream>

int main(int argc, char **argv)
try {
    for (int i = 0; i < argc; ++i)
        std::cout << "argv[" << i << "]=" << argv[i] << '\n';
    return 0;
} catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return 1;
}
